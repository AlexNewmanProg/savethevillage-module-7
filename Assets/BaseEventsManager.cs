﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets
{
    /// <summary>
    /// Базовый обработчик игровых событий
    /// </summary>
    public class BaseEventsManager : MonoBehaviour
    {
        /// <summary>
        /// Таймер игрового события
        /// </summary>
        [SerializeField]
        protected TimerWithIndicator EventTimer;

        /// <summary>
        /// Звук игрового события
        /// </summary>
        [SerializeField]
        private AudioSource EventAudio;

        /// <summary>
        /// Время через которое срабатывает игровое событие
        /// </summary>
        [SerializeField]
        protected int EventTime;

        /// <summary>
        /// Событие вызываемое после срабатывания
        /// </summary>
        public Action TimeIsUp;

        private void Update()
        {
            if (EventTimer.Tick)
            {
                EventAudio.Play();
                TimeIsUp?.Invoke();
            }
        }

        /// <summary>
        /// Запуск таймера
        /// </summary>
        public void StartTimer()
        {
            EventTimer.MaxTime = EventTime;
            EventTimer.IsRunning = true;
        }
    }
}
