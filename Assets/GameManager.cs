﻿using Assets;
using Assets.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    /// <summary>
    /// Текст ресурсов
    /// </summary>
    [SerializeField]
    private Text ResourcesText;

    /// <summary>
    /// Количество врагов на следующей волне
    /// </summary>
    [SerializeField]
    private Text NextWaveEnemyCount;

    /// <summary>
    /// Номер волны врагов
    /// </summary>
    [SerializeField]
    private Text WaveNumber;

    /// <summary>
    /// Текущее колличество пшеницы
    /// </summary>
    [SerializeField]
    private int WheatCount;

    /// <summary>
    /// Текущее колличество крестьян
    /// </summary>
    private int PeasantCount;

    /// <summary>
    /// Текущее колличество воинов
    /// </summary>
    private int WarrirorCount;

    /// <summary>
    /// Кнопка создания крестьянина
    /// </summary>
    [SerializeField]
    private Button PeasantCreationButton;

    /// <summary>
    /// Кнопка создания воина
    /// </summary>
    [SerializeField]
    private Button WarrirorCreationButton;

    /// <summary>
    /// Меню проигрыша
    /// </summary>
    [SerializeField]
    private GameObject GameoverScreen;

    /// <summary>
    /// Меню победы
    /// </summary>
    [SerializeField]
    private GameObject VictoryScreen;

    /// <summary>
    /// Меню паузы
    /// </summary>
    [SerializeField]
    private GameObject PauseScreen;

    /// <summary>
    /// Текст цели игры
    /// </summary>
    [SerializeField]
    private Text GameTaskText;

    /// <summary>
    /// Звук победы
    /// </summary>
    [SerializeField]
    private AudioSource VictorySound;

    /// <summary>
    /// Звук поражения
    /// </summary>
    [SerializeField]
    private AudioSource GameoverSound;

    /// <summary>
    /// Обработчик создания воинов
    /// </summary>
    private WarrirorManager WarrirorManager;

    /// <summary>
    /// Обработчик создания крестьян
    /// </summary>
    private PeasantManager PeasantManager;

    /// <summary>
    /// Обработчик урожаев
    /// </summary>
    private HarvestEventManager HarvestManager;

    /// <summary>
    /// Обработчик перекусов
    /// </summary>
    private FoodEventManager FoodManager;

    /// <summary>
    /// Обработчик боев
    /// </summary>
    private BattleManager BattleManager;

    /// <summary>
    /// Обработчик игровых задач
    /// </summary>
    private GameTaskManager GameTaskManager;

    void Start()
    {
        WarrirorManager = GetComponent<WarrirorManager>();
        WarrirorManager.CreatedAction += WarrirorCreated;

        WarrirorCount = WarrirorManager.Count;

        PeasantManager = GetComponent<PeasantManager>();
        PeasantManager.CreatedAction += PeasantCreated;

        PeasantCount = PeasantManager.Count;

        HarvestManager = GetComponent<HarvestEventManager>();
        HarvestManager.TimeIsUp += Harvest;
        HarvestManager.StartTimer();

        FoodManager = GetComponent<FoodEventManager>();
        FoodManager.TimeIsUp += FoodTime;
        FoodManager.StartTimer();

        BattleManager = GetComponent<BattleManager>();
        NextWaveEnemyCount.text = BattleManager.EnemiesCount.ToString();
        WaveNumber.text = BattleManager.EnemyWave.ToString();
        BattleManager.Attack += Attacked;

        GameTaskManager = GetComponent<GameTaskManager>();
        GameTaskManager.TaskCompleted += Victory;
        GameTaskText.text = GameTaskManager.TaskText;
    }

    void Update()
    {
        UpdateResoursesText();
        GameTaskText.text = GameTaskManager.TaskText;

        var peasantCreationStatus = PeasantManager.CreationStatus;

        if (WheatCount < PeasantManager.Cost
            || peasantCreationStatus.Equals(ItemStatus.CreationInProgress))
        {
            PeasantCreationButton.interactable = false;
        }
        else if(peasantCreationStatus.Equals(ItemStatus.Idle))
        {
            PeasantCreationButton.interactable = true;
        }

        var warrirorCreationStatus = WarrirorManager.CreationStatus;

        if (WheatCount < WarrirorManager.Cost
            || warrirorCreationStatus.Equals(ItemStatus.CreationInProgress))
        {
            WarrirorCreationButton.interactable = false;
        }
        else if(warrirorCreationStatus.Equals(ItemStatus.Idle))
        {
            WarrirorCreationButton.interactable = true;
        }
    }

    /// <summary>
    /// Создать крустьянина
    /// </summary>
    public void CreatePeasant()
    {
        var peasantCost = PeasantManager.Cost;

        if(WheatCount >= peasantCost
            && PeasantManager.CreationStatus.Equals(ItemStatus.Idle))
        {
            WheatCount -= peasantCost;
            PeasantManager.Create();
            PeasantCreationButton.interactable = false;
        }
    }

    /// <summary>
    /// Создать воина
    /// </summary>
    public void CreateWarriror()
    {
        var warrirorCost = WarrirorManager.Cost;

        if (WheatCount >= warrirorCost
            && WarrirorManager.CreationStatus.Equals(ItemStatus.Idle))
        {
            WheatCount -= warrirorCost;
            WarrirorManager.Create();
            WarrirorCreationButton.interactable = false;
        }
    }

    /// <summary>
    /// Событие созданного воина
    /// </summary>
    /// <param name="createdCount">Сколько воинов создано</param>
    private void WarrirorCreated(int createdCount)
    {
        GameTaskManager.UpdateWarrirorHiredCount(createdCount);
        WarrirorCount = WarrirorManager.Count;
        UpdateResoursesText();
    }

    /// <summary>
    /// Событие созданного крестьянина
    /// </summary>
    /// <param name="createdCount">Количество созданных крестьян</param>
    private void PeasantCreated(int createdCount)
    {
        GameTaskManager.UpdatePeasantHiredCount(createdCount);
        PeasantCount = PeasantManager.Count;
        UpdateResoursesText();
    }

    /// <summary>
    /// Событие урожая
    /// </summary>
    private void Harvest()
    {
        var wheatGrown = HarvestManager.UpdateWheatCount(PeasantCount, PeasantManager.WheatPerItem);
        WheatCount += wheatGrown;

        GameTaskManager.UpdateWheatGrownCount(wheatGrown);
        UpdateResoursesText();
    }

    /// <summary>
    /// Событие перекуса
    /// </summary>
    private void FoodTime()
    {
        WheatCount = FoodManager.UpdateWheatCount(WheatCount, WarrirorCount, WarrirorManager.WheatPerItem);

        if(WheatCount < 0)
        {
            WheatCount = 0;
            Gameover();
        }
    }

    /// <summary>
    /// Событие аттаки
    /// </summary>
    /// <param name="enemiesCount">Сколько врагов напало</param>
    private void Attacked(int enemiesCount)
    {
        var survivedWarrirors = WarrirorCount - enemiesCount;

        if(survivedWarrirors > 0)
        {
            WarrirorCount = survivedWarrirors;
            WarrirorManager.UnitDead(enemiesCount);
            GameTaskManager.UpdateEnemiesKilledCount(enemiesCount);
        }
        else
        {
            GameTaskManager.UpdateEnemiesKilledCount(enemiesCount);

            WarrirorCount = 0;
            Gameover();
        }
    }

    /// <summary>
    /// Обновляет табло с ресурсами
    /// </summary>
    private void UpdateResoursesText()
    {
        ResourcesText.text = $"Крестьян: {PeasantCount}\nВоинов: {WarrirorCount}\n\nПшеницы: {WheatCount}";
        NextWaveEnemyCount.text = BattleManager.EnemiesCount.ToString();
        WaveNumber.text = BattleManager.EnemyWave.ToString();
    }

    /// <summary>
    /// Открывает меню проигрыша
    /// </summary>
    public void Gameover()
    {
        GameoverSound.Play();
        GameoverScreen.SetActive(true);
        Time.timeScale = 0;
    }

    /// <summary>
    /// Открывает меню победы
    /// </summary>
    public void Victory()
    {
        VictorySound.Play();
        VictoryScreen.SetActive(true);
        Time.timeScale = 0;
    }

    /// <summary>
    /// Ставит на паузу игру
    /// </summary>
    public void Pause()
    {
        PauseScreen.SetActive(true);
        Time.timeScale = 0;
    }

    /// <summary>
    /// Продолжает игру
    /// </summary>
    public void Continue()
    {
        PauseScreen.SetActive(false);
        Time.timeScale = 1;
    }

    /// <summary>
    /// Перезупускает игру
    /// </summary>
    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
    }

    /// <summary>
    /// Открывает главное меню
    /// </summary>
    public void MainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu", LoadSceneMode.Single);
    }

    /// <summary>
    /// Включает/отключает звук
    /// </summary>
    public void Mute()
    {
        AudioListener.pause = !AudioListener.pause;
    }
}
