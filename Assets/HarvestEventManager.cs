﻿namespace Assets
{
    public class HarvestEventManager : BaseEventsManager
    {
        /// <summary>
        /// Считает сколько было выращено пшеницы
        /// </summary>
        /// <param name="currentPeasantsCount">Количество крестьян</param>
        /// <param name="wheatPerPeasant">Сколько выращивает за раз один крестьянин</param>
        /// <returns>Сколько выращено пшеницы</returns>
        public int UpdateWheatCount(int currentPeasantsCount, int wheatPerPeasant)
        {
            return currentPeasantsCount * wheatPerPeasant;
        }
    }
}
