﻿using UnityEngine;
using UnityEngine.UI;
using static UnityEngine.UI.Image;

public class TimerWithIndicator : MonoBehaviour
{
    /// <summary>
    /// Время отсчитываемое таймером
    /// </summary>
    [Min(0)]
    public float MaxTime;

    /// <summary>
    /// Главное изображение
    /// </summary>
    private Image _mainImage;

    /// <summary>
    /// Фоновое изображение
    /// </summary>
    private Image _fadedImage;

    /// <summary>
    /// Текущее время
    /// </summary>
    private float _currentTime;

    /// <summary>
    /// Метод заполнения изображения
    /// </summary>
    public FillMethod FillMethod;

    /// <summary>
    /// Указывает конец оттсчета таймера
    /// </summary>
    public bool Tick { get; private set; }

    /// <summary>
    /// Указывает запущен ли таймер
    /// </summary>
    public bool IsRunning = false;

    void Start()
    {
        _mainImage = GetComponent<Image>();

        _mainImage.fillMethod = FillMethod;
        _mainImage.fillClockwise = true;
        _mainImage.fillOrigin = 1;
    }


    void Update()
    {
        Tick = false;

        if (IsRunning)
        {
            _currentTime += Time.deltaTime;

            if (_currentTime >= MaxTime)
            {
                Tick = true;
                _currentTime = 0;
            }

            _mainImage.fillAmount = _currentTime / MaxTime;

        }
        else
        {
            _mainImage.fillAmount = 1;
        }
    }
}
