﻿using Assets.Scripts.Events;
using Assets.Scripts.Models;

namespace Assets.Scripts
{
    /// <summary>
    /// Tasks factory
    /// </summary>
    public static class TaskFactory
    {
        /// <summary>
        /// Creates task by type
        /// </summary>
        /// <param name="type">Task type</param>
        /// <param name="taskCompleteItemsCount">Items count to achieve goal</param>
        /// <returns>Game task</returns>
        public static GameTask CreateTask(TaskType type, int taskCompleteItemsCount)
        {
            switch (type)
            {
                case TaskType.KillEnemies:
                    return new EnemiesKillTask(taskCompleteItemsCount);
                case TaskType.GrowWheat:
                    return new GrowWheatTask(taskCompleteItemsCount);
                case TaskType.HirePeasant:
                    return new HirePeasantsTask(taskCompleteItemsCount);
                case TaskType.HireWarriror:
                    return new HireWarrirorsTask(taskCompleteItemsCount);
                default:
                    return new GameTask(taskCompleteItemsCount);
            }
        }
    }
}
