﻿namespace Assets.Scripts
{
    /// <summary>
    /// Тип игровой задачи
    /// </summary>
    public enum TaskType
    {
        HirePeasant,
        HireWarriror,
        KillEnemies,
        GrowWheat
    }
}
