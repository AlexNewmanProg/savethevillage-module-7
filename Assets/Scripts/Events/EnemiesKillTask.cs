﻿using Assets.Scripts.Models;

namespace Assets.Scripts.Events
{
    /// <summary>
    /// Событие при котором необходимо уничтожить определенное количество врагов
    /// </summary>
    public class EnemiesKillTask : GameTask
    {
        public EnemiesKillTask(int enemiesToKill) : base(enemiesToKill)
        {           
            // Просто для склонения
            var declensionText = (_achieveItemsCount < 5) ? "врага" : "врагов"; 

            EventTask = $"Уничтожить {_achieveItemsCount} {declensionText}";
        }
    }
}
