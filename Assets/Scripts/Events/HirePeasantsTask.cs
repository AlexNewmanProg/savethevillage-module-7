﻿using Assets.Scripts.Models;
using System;

namespace Assets.Scripts.Events
{
    /// <summary>
    /// Событие при котором нужно нанять определенное количество крестьян
    /// </summary>
    public class HirePeasantsTask : GameTask
    {
        public HirePeasantsTask(int peasantToHire) : base(peasantToHire)
        {
            // Просто для склонения
            var declensionText = (_achieveItemsCount < 2) ? "крестьянина" : "крестьян";

            EventTask = $"Нанять {_achieveItemsCount} {declensionText}";
        }
    }
}
