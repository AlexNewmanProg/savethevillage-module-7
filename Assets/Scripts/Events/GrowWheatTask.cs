﻿using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Events
{
    /// <summary>
    /// Событие при котором необходимо вырастить определенное количество пшеницы
    /// </summary>
    public class GrowWheatTask : GameTask
    {
        public GrowWheatTask(int wheatToGrow) : base(wheatToGrow)
        {
            // Просто для склонения
            var declensionText = (_achieveItemsCount > 1)
                                       ? (_achieveItemsCount < 5) ? "единицы" : "единиц" 
                                                : "единицу";

            EventTask = $"Вырастить {_achieveItemsCount} {declensionText}";
        }
    }
}
