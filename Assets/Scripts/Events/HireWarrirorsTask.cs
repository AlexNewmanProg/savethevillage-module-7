﻿using Assets.Scripts.Models;
using System;

namespace Assets.Scripts.Events
{
    /// <summary>
    /// Событие при котором необходимо нанять определенное количество воинов
    /// </summary>
    public class HireWarrirorsTask : GameTask
    {
        public HireWarrirorsTask(int peasantToHire) : base(peasantToHire)
        {
            // Просто для склонения
            var declensionText = (_achieveItemsCount < 5) ? "воина" : "воинов";

            EventTask = $"Нанять {_achieveItemsCount} {declensionText}";
        }
    }
}
