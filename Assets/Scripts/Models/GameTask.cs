﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Assets.Scripts.Models
{
    /// <summary>
    /// Описывает базовое игровое событие
    /// </summary>
    public class GameTask
    {
        /// <summary>
        /// Текущее значение величины
        /// </summary>
        public int ItemsCount 
        { 
            get 
            { 
                return _itemsCount;
            }
        }

        /// <summary>
        /// Текущее значение величины
        /// </summary>
        protected int _itemsCount;

        /// <summary>
        /// Необходимое значение величины
        /// </summary>
        public int AchieveItemsCount
        {
            get
            {
                return _achieveItemsCount;
            }
        }

        /// <summary>
        /// Необходимое значение величины
        /// </summary>
        protected int _achieveItemsCount;

        /// <summary>
        /// Цель события
        /// </summary>
        public string EventTask 
        { 
            get 
            {
                return _eventTask;
            }
            set
            {
                _eventTask = $"{value} ({_itemsCount}/{_achieveItemsCount})";
            }
        }

        protected string _eventTask;

        /// <summary>
        /// Событие при достижении цели
        /// </summary>
        public virtual event Action TaskCompleted;

        public GameTask(int achieveItemsCount)
        {
            _achieveItemsCount = (achieveItemsCount > 0) ? achieveItemsCount : 1;
        }

        /// <summary>
        /// Обновление величины
        /// </summary>
        /// <param name="value">На сколько изменилось значение</param>
        public virtual void UpdateTaskData(int value)
        {
            _itemsCount += value;

            _eventTask = Regex.Replace(_eventTask, @"\(\d*\/\d*\)", string.Empty);
            _eventTask = $"{_eventTask.Trim()} ({_itemsCount}/{_achieveItemsCount})";

            if (_itemsCount >= _achieveItemsCount)
            {
                TaskCompleted?.Invoke();
            }
        }
    }
}
