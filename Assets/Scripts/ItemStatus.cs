﻿namespace Assets.Scripts
{
    /// <summary>
    /// Статусы создания юнита
    /// </summary>
    public enum ItemStatus
    {
        Idle,
        CreationInProgress
    }
}
