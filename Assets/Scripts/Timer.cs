﻿using UnityEngine;

public class Timer : MonoBehaviour
{
    /// <summary>
    /// Время отсчитываемое таймером
    /// </summary>
    [Min(0)]
    public float MaxTime;

    /// <summary>
    /// Текущее время
    /// </summary>
    private float _currentTime;

    /// <summary>
    /// Указывает конец оттсчета таймера
    /// </summary>
    public bool Tick { get; private set; }

    /// <summary>
    /// Указывает запущен ли таймер
    /// </summary>
    public bool IsRunning;

    public Timer()
    {
        IsRunning = false;
    }
    
    void Update()
    {
        Tick = false;

        if (IsRunning)
        {
            _currentTime += Time.deltaTime;

            if (_currentTime >= MaxTime)
            {
                Tick = true;
                _currentTime = 0;
            }
        }
    }
}
