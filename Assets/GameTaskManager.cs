﻿using Assets.Scripts;
using Assets.Scripts.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Обработчик игровых целей
/// </summary>
public class GameTaskManager : MonoBehaviour
{
    /// <summary>
    /// Сколько нанять крестьян
    /// </summary>
    public int HirePeasantCount;

    /// <summary>
    /// Сколько нанять воинов
    /// </summary>
    public int HireWarrirorCount;

    /// <summary>
    /// Сколько уничтожить врагов
    /// </summary>
    public int KillEnemiesCount;

    /// <summary>
    /// Сколько вырастить пшеницы
    /// </summary>
    public int GrowWheatCount;

    /// <summary>
    /// Текст цели
    /// </summary>
    [HideInInspector]
    public string TaskText { get; private set; }

    /// <summary>
    /// Список обработчиков целей
    /// </summary>
    private Dictionary<TaskType, int> _events = new Dictionary<TaskType, int>();

    /// <summary>
    /// Событие вызываемое при достижении цели
    /// </summary>
    public Action TaskCompleted;

    /// <summary>
    /// Текущий тип цели
    /// </summary>
    private TaskType _currentEventType;

    /// <summary>
    /// Текущий обработчик цели
    /// </summary>
    private GameTask _currentEvent;

    /// <summary>
    /// Просто рандомное число для выбора цели
    /// </summary>
    private int _randomEventNumber;

    public void Start()
    {
        _randomEventNumber = UnityEngine.Random.Range(0, 3);

        _events.Add(TaskType.KillEnemies, KillEnemiesCount);
        _events.Add(TaskType.HirePeasant, HirePeasantCount);
        _events.Add(TaskType.HireWarriror, HireWarrirorCount);
        _events.Add(TaskType.GrowWheat, GrowWheatCount);

        ChooseTask();
    }

    /// <summary>
    /// Выбирает рандомно цель для игры
    /// </summary>
    private void ChooseTask()
    {
        var eventInfo = _events.ElementAt(_randomEventNumber);
        _currentEventType = eventInfo.Key;

        _currentEvent = TaskFactory.CreateTask(_currentEventType, eventInfo.Value);
        _currentEvent.TaskCompleted += TaskCompletedAction;

        TaskText = _currentEvent.EventTask;
    }
    
    public void Update()
    {
        TaskText = _currentEvent.EventTask;
    }

    /// <summary>
    /// Событие вызываемое при достижении цели
    /// </summary>
    public void TaskCompletedAction()
    {
        TaskCompleted?.Invoke();
    }

    /// <summary>
    /// Обновляет отслеживаемое значение 
    /// </summary>
    /// <param name="value">Новое значение</param>
    private void UpdateTaskData(int value)
    {
        _currentEvent.UpdateTaskData(value);
    }

    /// <summary>
    /// Обновляет количество нанятых крестьян
    /// </summary>
    /// <param name="value">Значение</param>
    public void UpdatePeasantHiredCount(int value)
    {
        if (_currentEventType.Equals(TaskType.HirePeasant))
        {
            UpdateTaskData(value);
        }
    }

    /// <summary>
    /// Обновляет количество нанятых воинов
    /// </summary>
    /// <param name="value">Значение</param>
    public void UpdateWarrirorHiredCount(int value)
    {
        if (_currentEventType.Equals(TaskType.HireWarriror))
        {
            UpdateTaskData(value);
        }
    }

    /// <summary>
    /// Обновляет количество уничтоженных врагов
    /// </summary>
    /// <param name="value">Значение</param>
    public void UpdateEnemiesKilledCount(int value)
    {
        if (_currentEventType.Equals(TaskType.KillEnemies))
        {
            UpdateTaskData(value);
        }
    }

    /// <summary>
    /// Обновляет количество выращенной пшеницы
    /// </summary>
    /// <param name="value">Значение</param>
    public void UpdateWheatGrownCount(int value)
    {
        if (_currentEventType.Equals(TaskType.GrowWheat))
        {
            UpdateTaskData(value);
        }
    }
}
