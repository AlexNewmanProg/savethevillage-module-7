﻿using Assets.Scripts;
using System;
using UnityEngine;

namespace Assets
{
    /// <summary>
    /// Базовый класс для создания юнитов
    /// </summary>
    public class BaseCreationItemsManager : MonoBehaviour
    {
        /// <summary>
        /// Текущее количество юнитов
        /// </summary>
        [Min(0)]
        [SerializeField]
        private int _count;

        /// <summary>
        /// Текущее количество юнитов
        /// </summary>
        public int Count { get { return _count; } }

        /// <summary>
        /// Количество пшеницы для юнита
        /// </summary>
        [Min(0)]
        [SerializeField]
        private int _wheatPerItem;

        /// <summary>
        /// Количество пшеницы для юнита
        /// </summary>
        public int WheatPerItem { get { return _wheatPerItem; } }

        [SerializeField]
        private TimerWithIndicator CreationTimer;

        /// <summary>
        /// Время создания юнита
        /// </summary>
        [Min(1)]
        [SerializeField]
        private int _creationTime;

        /// <summary>
        /// Время создания юнита
        /// </summary>
        public int CreationTime { get { return _creationTime; } }

        /// <summary>
        /// Стоимость юнита
        /// </summary>
        [Min(1)]
        [SerializeField]
        private int _cost;

        /// <summary>
        /// Стоимость юнита
        /// </summary>
        public int Cost { get { return _cost; } }
                
        /// <summary>
        /// Статус создания юнита
        /// </summary>
        [HideInInspector]
        public ItemStatus CreationStatus { get; private set; }

        /// <summary>
        /// Звук создания юнита
        /// </summary>
        [SerializeField]
        private AudioSource CreatedSound;
        
        /// <summary>
        /// Событие вызываемое когда юнит создан
        /// </summary>
        [HideInInspector]
        public Action<int> CreatedAction;

        private void Update()
        {
            if (CreationTimer.Tick)
            {
                _count += 1;
                CreationTimer.IsRunning = false;
                CreationStatus = ItemStatus.Idle;
                CreatedSound.Play();
                CreatedAction?.Invoke(1);
            }
        }

        /// <summary>
        /// Создать юнита
        /// </summary>
        public void Create()
        {
            CreationTimer.MaxTime = _creationTime;
            CreationTimer.IsRunning = true;
            CreationStatus = ItemStatus.CreationInProgress;
        }

        /// <summary>
        /// Уменьшение количества юнитов
        /// </summary>
        /// <param name="deadCount">На сколько уменьшить</param>
        public void UnitDead(int deadCount)
        {
            _count -= deadCount;
        }
    }
}
