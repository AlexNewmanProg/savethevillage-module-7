﻿namespace Assets
{
    public class FoodEventManager : BaseEventsManager
    {
        /// <summary>
        /// Обновить количество пшеницы
        /// </summary>
        /// <param name="currentWheatCount">Текущее кол-во пшеницы</param>
        /// <param name="currentWarrirorsCount">Текущее кол-во воинов</param>
        /// <param name="wheatPerWarriror">Количество пшеницы для одного воина</param>
        /// <returns>Измененное количество пшеницы</returns>
        public int UpdateWheatCount(int currentWheatCount, int currentWarrirorsCount, int wheatPerWarriror)
        {
            var wheatEaten = currentWarrirorsCount * wheatPerWarriror;
            return currentWheatCount - wheatEaten;
        }
    }
}
