﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets
{
    /// <summary>
    /// Обрабатывает события в главном меню
    /// </summary>
    public class MenuManager : MonoBehaviour
    {
        /// <summary>
        /// Тапускает игру
        /// </summary>
        public void StartGame()
        {
            SceneManager.LoadScene("GameScene", LoadSceneMode.Single);
        }
    }
}
