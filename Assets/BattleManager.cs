﻿using System;
using UnityEngine;

/// <summary>
/// Обработчик сражений
/// </summary>
public class BattleManager : MonoBehaviour
{
    /// <summary>
    /// Таймер атак
    /// </summary>
    public TimerWithIndicator RaidTimer;

    /// <summary>
    /// Звук атаки
    /// </summary>
    [SerializeField]
    private AudioSource RaidSound;

    /// <summary>
    /// Звук обнаружения врагов
    /// </summary>
    [SerializeField]
    private AudioSource EnemieSightedSound;

    /// <summary>
    /// Время через которое начинаются аттаки
    /// </summary>
    [SerializeField]
    private int RaidDelay;

    /// <summary>
    /// Таймер отсрочки атак
    /// </summary>
    private Timer RaidDelayTimer;

    /// <summary>
    /// Время через которое происходит атака
    /// </summary>
    [SerializeField]
    private float RaidTime;

    /// <summary>
    /// Враги
    /// </summary>
    private int[] Enemies;

    /// <summary>
    /// Номер текущей волны
    /// </summary>
    private int _enemyWave;

    /// <summary>
    /// Номер текущей волны
    /// </summary>
    public int EnemyWave
    {
        get
        {
            return _enemyWave;
        }
    }

    /// <summary>
    /// Количество врагов на текущей волне
    /// </summary>
    public int EnemiesCount
    {
        get
        {
            return Enemies[_enemyWave];
        }
    }

    /// <summary>
    /// Событие атаки
    /// </summary>
    public Action<int> Attack;

    public BattleManager()
    {
        GenerateEnemies();
        _enemyWave = 0;
    }

    // Start is called before the first frame update
    void Start()
    {
        RaidTimer.IsRunning = false;
        RaidTimer.MaxTime = RaidTime;

        RaidDelayTimer = gameObject.AddComponent<Timer>();
        RaidDelayTimer.MaxTime = (float) TimeSpan.FromSeconds(RaidDelay).TotalSeconds;
        RaidDelayTimer.IsRunning = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (RaidTimer.Tick)
        {
            RaidSound.Play();
            Attack?.Invoke(EnemiesCount);

            _enemyWave++;

            RaidTimer.MaxTime = RaidTime;
        }

        if (RaidDelayTimer.Tick)
        {
            EnemieSightedSound.Play();
            RaidTimer.MaxTime = RaidTime + RaidDelay;
            RaidTimer.IsRunning = true;

            RaidDelayTimer.IsRunning = false;
        }
    }

    /// <summary>
    /// "Генерирует" врагов
    /// </summary>
    void GenerateEnemies()
    {
        Enemies = new int[] { 1, 2, 2, 3, 3, 4, 3, 4, 5, 5, 4, 3, 2, 3, 5 };
    }
}
